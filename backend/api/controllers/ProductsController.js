module.exports = {
  getProducts: async (req, res) => {
    const page = req.param('page')

    const amountOfProducts = await Product
      .count()
      .catch(error => res.serverError(error))

    const products = await Product
      .find()
      .paginate({page, limit: 6})
      .catch(error => res.serverError(error))
    const users = await User.find();

    // Match user with products
    for(let i = 0; i < products.length; i++){
      let currentProduct = products[i];
      for(let x = 0; x < users.length; x++){
        const currentUser = users[x];
        if(currentProduct.user === users[x].number){
          products[i].user = currentUser;
        }
      }
    }
    return res.json({
      products,
      amountOfProducts
    })
  }
}
