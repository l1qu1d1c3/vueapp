module.exports = {
  attributes: {
    id: {
      type: 'string',
      primaryKey: true
    },
    title: {
      type: 'string',
      required: true
    },
    description: {
      type: 'string'
    },
    price: {
      type: 'float',
      required: true
    },

    user: {
      model: 'user'
    }
  }
}
