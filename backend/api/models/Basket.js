module.exports = {
  attributes: {
    id:{
      type:'string',
      primaryKey: true
    },
    products: {
      type: 'json',
      required: true
    },

    user: {
      model: 'user'
    }
  }
}
